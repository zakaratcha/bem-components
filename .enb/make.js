var Mocha = require('mocha'),

    levels = require('enb-bem-techs/techs/levels'),
    provide = require('enb/techs/file-provider'),
    bemdecl = require('enb-bem-techs/techs/bemjson-to-bemdecl'),
    deps = require('enb-bem-techs/techs/deps-old'),
    files = require('enb-bem-techs/techs/files'),
    bemhtml = require('enb-bemxjst/techs/bemhtml-old'),
    html = require('enb-bemxjst/techs/html-from-bemjson'),
    browserJs = require('enb-diverse-js/techs/browser-js'),
    vanillaJs = require('enb-diverse-js/techs/vanilla-js'),
    ym = require('enb-modules/techs/prepend-modules'),
    css = require('enb-stylus/techs/css-stylus'),
    borschik = require('enb-borschik/techs/borschik'),
    autoprefixer = require('enb-autoprefixer/techs/css-autoprefixer'),
    depsByTechToBemdecl = require('enb-bem-techs/techs/deps-by-tech-to-bemdecl'),
    mergeFiles = require('enb/techs/file-merge'),
    mergeBemdecl = require('enb-bem-techs/techs/merge-bemdecl'),

    pathSep = process.platform === 'win32'? '\\' : '/';

module.exports = function(config) {
    config.task('unit', function(task) {
        var scanner = require('enb-bem-pseudo-levels/lib/level-scanner'),
            vow = require('enb/node_modules/vow'),
            vfs = require('enb/node_modules/vow-fs');

        var targetNodes = Array.prototype.slice.call(arguments, 1),
            defer = require('vow').defer();

        if(!targetNodes.length) {
            scanner.scan(['common.blocks'].map(function(level) {
                return { path : config.resolvePath(level) };
            })).then(function(sourceFiles) {
                    defer.resolve([].concat(sourceFiles).filter(function(file) {
                        return file.suffix === 'unit.js';
                    }).map(function(file) {
                            return require('path').join('desktop.tests',
                                file.name.substr(0, file.name.length - 1 - file.suffix.length));
                        }));
                });
        } else defer.resolve(targetNodes);

        return defer.promise().then(function(nodes) {
            return vow.all(nodes.map(function(node) {
                    return vfs.makeDir(require('path').join(config.getRootPath(), node));
                }))
                .then(function() {
                    config.nodes(['desktop.tests' + pathSep + '*'], function(nodeConfig) {
                        nodeConfig.addTechs([
                            [levels, { levels : getLevels(config) }],
                            [require('enb/lib/build-flow').create()
                                .name('bemdecl.js')
                                .target('target', '?.bemdecl.js')
                                .builder(function() { return 'exports.blocks = [{ "name": "' + nodeConfig._baseName + '" }]'; })
                                .createTech()],
                            [deps],
                            [files],

                            [vanillaJs],
                            [require('enb/lib/build-flow').create()
                                .name('unit-test')
                                .target('target', '?.unit.js')
                                .useFileList(['vanilla.js', 'unit.js'])
                                .builder(function(sourceFiles) {
                                    return this._joinFiles(sourceFiles.filter(function(file) {
                                        return file.suffix === 'unit.js' ?
                                            file.name === nodeConfig._baseName + '.unit.js' :
                                            true;
                                    }));
                                })
                                .createTech()],
                            [ym, { source : '?.unit.js', target : '_?.unit.js' }]
                        ]);

                        nodeConfig.addTargets(['_?.unit.js']);
                    });

                    return task._makePlatform.buildTargets(nodes)
                        .then(function() {
                            var args = arguments,
                                mocha = new Mocha({ reporter : 'nyan' }),
                                defer = require('vow').defer();

                            nodes.forEach(function(node) {
                                mocha.addFile(node + pathSep + '_' + node.split(pathSep).pop() + '.unit.js');
                            });

                            mocha.run(function(failures) {
                                if(failures !== 0) defer.reject();

                                defer.resolve.apply(defer, args);
                            });

                            return defer.promise();
                        });
                });
        });
    });

    config.includeConfig('enb-bem-examples');
    config.includeConfig('enb-bem-docs');
    config.includeConfig('enb-bem-specs');

    var examples = config.module('enb-bem-examples').createConfigurator('examples'),
        docs = config.module('enb-bem-docs').createConfigurator('docs'),
        specs = config.module('enb-bem-specs').createConfigurator('specs');

    config.setLanguages(['ru']);

    examples.configure({
        destPath : 'desktop.examples',
        levels : ['common.blocks']
    });

    docs.configure({
        destPath : 'desktop.docs',
        levels : ['common.blocks'],
        exampleSets : ['desktop.examples'],
        langs : config.getLanguages(),
        jsdoc : { suffixes : ['vanilla.js', 'browser.js', 'js'] }
    });

    specs.configure({
        destPath : 'desktop.specs',
        levels : ['common.blocks'],
        sourceLevels : [].concat(getLevels(config)),
        jsSuffixes : ['vanilla.js', 'browser.js', 'js']
    });

    config.nodes(['desktop.examples/*/*'], function(nodeConfig) {
        nodeConfig.addTechs([
            [levels, { levels : getLevels(config) }],
            [bemdecl],
            [deps],
            [files],
//            [provide, { target : '?.bemjson.js' }],

            [bemhtml, { devMode : false }],
            [html],

            [css, { target : '?.noprefix.css', filesTarget : '?.files' }],
            [autoprefixer, {
                sourceTarget : '?.noprefix.css',
                destTarget : '?.prefix.css',
                browserSupport : [
                    'last 2 versions',
                    'ie 10',
                    'ff 24',
                    'opera 12.16'
                ]
            }],

            [depsByTechToBemdecl, {
                target : '?.js-tech-bemdecl.js',
                sourceTech : 'js',
                destTech : 'js'
            }],
            [mergeBemdecl, {
                sources : ['?.bemdecl.js', '?.js-tech-bemdecl.js'],
                target : '?.js-bemdecl.js'
            }],
            [deps, {
                bemdeclFile : '?.js-bemdecl.js',
                target : '?.js-deps.js'
            }],
            [files, {
                depsFile : '?.js-deps.js',
                filesTarget : '?.js.files',
                dirsTarget : '?.js.dirs'
            }],

            [depsByTechToBemdecl, {
                target : '?.bemhtml.bemdecl.js',
                sourceTech : 'js',
                destTech : 'bemhtml'
            }],
            [deps, {
                bemdeclFile : '?.bemhtml.bemdecl.js',
                target : '?.bemhtml.deps.js'
            }],
            [files, {
                depsFile : '?.bemhtml.deps.js',
                filesTarget : '?.bemhtml.files',
                dirsTarget : '?.bemhtml.dirs'
            }],
            [bemhtml, {
                target : '?.client.bemhtml.js',
                filesTarget : '?.bemhtml.files',
                devMode : false
            }],

            [vanillaJs],
            [browserJs, { filesTarget : '?.js.files' }],
            [ym, { source : '?.browser.js', target : '?.ym.js' }],
            [mergeFiles, {
                target : '?.client.js',
                sources : ['?.ym.js', '?.client.bemhtml.js']
            }]
        ]);

        nodeConfig.mode('development', function(nodeConfig) {
            nodeConfig.addTechs([
                [borschik, { source : '?.prefix.css', target : '_?.css', minify : false }],
                [borschik, { source : '?.client.js', target : '_?.js', minify : false }]
            ]);
        });

        nodeConfig.mode('production', function(nodeConfig) {
            nodeConfig.addTechs([
                [borschik, { source : '?.prefix.css', target : '_?.css', freeze : true, tech : 'cleancss' }],
                [borschik, { source : '?.client.js', target : '_?.js', freeze : true }]
            ]);
        });

        nodeConfig.addTargets(['_?.css', '_?.js', '?.html']);
    });
};

function getLevels(config) {
    return [
        'libs/bem-core/common.blocks',
        'libs/bem-core/desktop.blocks',
        'libs/bem-components/common.blocks',
        'libs/bem-components/desktop.blocks',
        'libs/bem-components/design/common.blocks',
        'libs/bem-components/design/desktop.blocks',
        'common.blocks',
        'design/common.blocks'
    ].map(function(levelPath) { return config.resolvePath(levelPath); });
}
