({
    block : 'page',
    js : true,
    title : 'Desktop blocks',
    head : [{ elem : 'css', url : '_simple.css' }],
    scripts : [{ elem : 'js', url : '_simple.js' }],
    mods : { theme : 'islands' },
    content : [
        {
            block : 'input',
            mods : { 'has-clear' : true, size : 'xl', theme : 'islands', type : 'date-time' },
            placeholder : 'дд.мм.гггг 00:00'
        },
        { tag : 'br' },
        { tag : 'br' },
        { tag : 'br' },
        {
            block : 'input',
            mods : { 'has-clear' : true, size : 'xl', theme : 'islands', type : 'date' },
            placeholder : 'дд.мм.гггг'
        },
        { tag : 'br' },
        { tag : 'br' },
        { tag : 'br' },
        {
            block : 'input',
            js : {
                mask : [
                    function(value) { return '012'.indexOf(value) > -1 ? value : 0; },
                    function(value, index, array) {
                        return '01234'.concat(array[index - 1] < 2 ? '56789' : '').indexOf(value) > -1 ? value : 0;
                    },
                    function() { return ':'; },
                    function(value) { return '012345'.indexOf(value) > -1 ? value : 0; },
                    function(value) { return '0123456189'.indexOf(value) > -1 ? value : 0; }
                ]
            },
            mods : { 'has-clear' : true, /*mask : true,*/ size : 'xl', theme : 'islands', type : 'time' },
            placeholder : '00:00'
        }
    ]
})
