([
    {
        tech : 'js',
        shouldDeps : [
            { block : 'i-bem', tech : 'bemhtml' },
            { block : 'calendar', tech : 'bemhtml' },
            { block : 'popup', mods : { theme : 'islands' }, tech : 'bemhtml' }
        ]
    },
    {
        shouldDeps : [
            'calendar',
            { block : 'functions', elem : 'throttle' },
            { block : 'popup', mods : { autoclosable : true, target : 'anchor', theme : 'islands' } }
        ]
    }
])
