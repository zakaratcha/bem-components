describe('Declension', function(){
    var m = modules,
        block;

    before(function(done) {
        m.require('declension', function() {
            block = arguments[0];

            done();
        });
    });

    var noun = {
            partOfSpeech : 'noun',
            nominative : { singular : 'минута', plural : 'минуты' },
            genitive : { singular : 'минуты', plural : 'минут' },
            dative : { singular : 'минуте', plural : 'минутам' },
            accusative : { singular : 'минуту', plural : 'минуты' },
            instrumental : { singular : 'минутой', plural : 'минутами' },
            prepositional : { singular : 'минуте', plural : 'минутах' },
            locative : { singular : 'минуте', plural : 'минутах' }
        },
        adjectiveMuscular = {
            partOfSpeech : 'adjective',
            gender : 'muscular',
            nominative : { singular : 'зеленый', plural : 'зеленые' },
            genitive : { singular : 'зеленого', plural : 'зеленых' },
            dative : { singular : 'зеленому', plural : 'зеленым' },
            accusative : { singular : 'зеленого', plural : 'зеленых' },
            instrumental : { singular : 'зеленым', plural : 'зелеными' },
            prepositional : { singular : 'зеленых', plural : 'зеленых' },
            locative : { singular : 'зеленых', plural : 'зеленых' }
        },
        adjectiveFeminine = {
            partOfSpeech : 'adjective',
            gender : 'feminine',
            nominative : { singular : 'зеленая', plural : 'зеленые' },
            genitive : { singular : 'зеленой', plural : 'зеленых' },
            dative : { singular : 'зеленой', plural : 'зеленым' },
            accusative : { singular : 'зеленую', plural : 'зеленых' },
            instrumental : { singular : 'зеленой', plural : 'зелеными' },
            prepositional : { singular : 'зеленой', plural : 'зеленых' },
            locative : { singular : 'зеленой', plural : 'зеленых' }
        },
        allCases = Object.keys(noun).slice(1),
        check = function(nums, cases, word, cb) {
            nums.forEach(function(num) {
                cases.forEach(function(value) {
                    cb(num, value, block(word, num, value));
                });
            });
        };

    // TODO: проверить в правилах
    it('Дробные числительные управляют формой В.п. мн. числа существительного', function() {
        check([0.1, 0.2, 0.5, 0.6], allCases, noun, function(num, caseName, value) {
            value.should.equal(noun.accusative.plural);
        });
    });
    // TODO: проверить в правилах
    it('Дробные числительные управляют формой В.п. ед. числа прилагательного', function() {
        check([0.1, 0.2, 0.5, 0.6], allCases, adjectiveMuscular, function(num, caseName, value) {
            value.should.equal(adjectiveMuscular.accusative.singular);
        });
    });
    it('Числительное один (одна, одно) согласуется с существительным в роде, числе и падеже', function() {
        check([1], allCases, noun, function(num, caseName, value) {
            value.should.equal(noun[caseName].singular);
        });
    });
    describe('Числительные два, три, четыре', function() {
        describe('в форме И.п. (и В. п. при неодушевленных существительных)', function() {
            it('управляют формой Р.п. единственного числа существительных', function() {
                check([2, 3, 4], ['nominative', 'accusative'], noun, function(num, caseName, value) {
                    value.should.equal(noun.genitive.singular);
                });
            });
        });
        describe('в остальных падежах', function() {
            it('согласуются с существительными во множественном числе', function() {
                check([2, 3, 4], ['genitive', 'dative', 'instrumental', 'prepositional', 'locative'], noun,
                    function(num, caseName, value) { value.should.equal(noun[caseName].plural); });
            });
        });
        describe('в сочетании с субстантивированными прилагательными', function() {
            it('мужского и среднего рода употребляются в форме Р.п. мн.ч.', function() {
                check([2, 3, 4], allCases, adjectiveMuscular, function(num, caseName, value) {
                    value.should.equal(adjectiveMuscular.genitive.plural);
                });
            });
            it('женского рода употребляются в форме Р. или И.-В. п. множественного числа', function() {
                check([2, 3, 4], allCases, adjectiveFeminine, function(num, caseName, value) {
                    ['nominative', 'genitive', 'dative', 'accusative'].some(function(caseName) {
                        return value === adjectiveFeminine[caseName].plural;
                    }).should.equal(true);
                });
            });
        });
    });
    describe('Числительные, начиная от пяти и далее', function() {
        it('в И.-В. п. управляют формой Р.п. множественного числа существительных', function() {
            check([5, 6], ['nominative', 'genitive', 'dative', 'accusative'], noun,
                function(num, caseName, value) { value.should.equal(noun.genitive.plural); });
        });
        it('в остальных падежах согласуются с существительными во множественном числе', function() {
            check([5, 6], ['instrumental', 'prepositional', 'locative'], noun,
                function(num, caseName, value) { value.should.equal(noun[caseName].plural); });
        });
    });
});
