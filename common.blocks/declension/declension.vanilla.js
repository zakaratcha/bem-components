/** @class declension */
modules.define('declension', [], function(provide) {
    /**
     * Возвращает соответствующую числительному и выбранному падежу форму слова (напр. (5, минута, Р.п.) -> "минут")
     * @param {Object} data Данные склоняемого слова
     * @param {String} data.partOfSpeech Часть речи (noun|adjective)
     * @param {String} data.gender Род (masculine|feminine|neuter)
     * @param {String} data.nominative Именительный падеж
     * @param {String} data.nominative.singular Единственное число
     * @param {String} data.nominative.plural Множественное число
     * @param {String} data.genitive Родительный
     * @param {String} data.dative Дательный
     * @param {String} data.accusative Винительный
     * @param {Number} [num] Кол-во, если нужно склонять учитывая числительное
     * @param {String} [cName] Названия целевого падежа
     */
    provide(function(data, num, cName) {
        var caseName = cName || (typeof num === 'string' && num),
            number = isFinite(+num) && num ? +num : 1,
            isFraction = number % 1 !== 0,
            significantFigures = Number(String(number).split('.')[isFraction ? 1 : 0].substr(-2)),
            significantLastFigure = Number(String(significantFigures).substr(-1));

        if(isFraction) return data.accusative[data.partOfSpeech === 'noun' ? 'plural' : 'singular'];
        // Числительное один (одна, одно) согласуется с существительным в роде, числе и падеже
        // (ср.: один день, одним днем, в одну неделю и т.д.).
        if((number > 20 ? significantLastFigure : number) === 1) return data[caseName].singular;
        // Числительные два, три, четыре в форме И.п. (и В. п. при неодушевленных существительных)
        // управляют формой Р.п. единственного числа существительных: два дня, два товарища, три окна.
        // В остальных падежах эти числительные согласуются с существительными во множественном числе.
        if([2, 3, 4].indexOf(number > 20 ? significantLastFigure : number) > -1) {
            // Субстантивированные прилагательные мужского и среднего рода в сочетании с числительными два, три, четыре
            // употребляются в форме Р.п. мн.ч. (ср.: два дежурных, три насекомых), а субстантивированные прилагательные
            // женского рода - в форме Р. или И.-В. п. множественного числа (ср.: две запятых - две запятые и т.п.).
            if(data.partOfSpeech === 'adjective') {
                if(caseName === 'nominative' && ['masculine', 'neuter'].indexOf(data.gender) > -1) {
                    return data.genitive.plural;
                } else {
                    return data[['nominative', 'genitive', 'dative', 'accusative'].indexOf(caseName) > -1 ?
                        caseName : 'genitive'].plural;
                }
            } else if(['nominative', 'accusative'].indexOf(caseName) > -1) {
                return data.genitive.singular;
            } else {
                return data[caseName].plural;
            }
        }
        // Числительные, начиная от пяти и далее в И.-В. п. управляют формой Р.п. множественного числа существительных,
        // а в остальных падежах – согласуются с существительными во множественном числе.
        if(['nominative', 'genitive', 'dative', 'accusative'].indexOf(caseName) > -1) {
            return data.genitive.plural;
        } else return data[caseName].plural;
    });
});
