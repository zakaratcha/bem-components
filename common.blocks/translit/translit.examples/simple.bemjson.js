({
    block : 'page',
    js : true,
    title : 'Translit',
    mods : { theme : 'islands' },
    head : [{ elem : 'css', url : '_simple.css' }],
    scripts : [{ elem : 'js', url : '_simple.js' }],
    content : [
        { block : 'textarea', mods : { size : 'm', theme : 'islands' }, placeholder : 'Введите текст' },
        { block : 'translit' }
    ]
})
