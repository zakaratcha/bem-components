/** @class form */
modules.define('form', [
    'i-bem__dom', 'jquery', 'vow'
], function(provide, BEMDOM, $, vow) {
    provide(BEMDOM.decl(this.name, /** @lends form.prototype */{
        onSetMod : {
            js : {
                inited : function() {
                    var _this = this,
                        changeHandler = function() {
                            _this.emit('change');
                        };

                    // подписка на change бэм-контролов
                    [
                        'checkbox-group', 'dnd-attach', 'input', 'radio-group', 'select', 'textarea'
                    ].forEach(function(blockName) {
                        BEMDOM.blocks[blockName] && BEMDOM.blocks[blockName].on(this.domElem, 'change', changeHandler);
                    }, this);

                    // подписка на change нативных контролов
                    ['input', 'select', 'textarea'].forEach(function(tagName) {
                        this.domElem.on('change', tagName, function(e) {
                            // если событие произошло не на бэм-контроле
                            if($(e.currentTarget).parents('.' + (({
                                text : 'input',
                                'select-one' : 'select',
                                'select-multiple' : 'select'
                            })[e.currentTarget.type] || e.currentTarget.type)).length === 0) changeHandler(e);
                        });
                    }, this);
                }
            }
        },

        _getFormElementsByName : function(name) {
            return Array.prototype.reduce.call(this.domElem[0].elements, function(prev, elem) {
                return elem.name === name ? prev.concat(elem) : prev;
            }, []);
        },

        _getBlocksByName : function(name) {
            var rez = [];

            ['checkbox', 'input', 'radio', 'select', 'dnd-attach'].forEach(function(blockName) {
                (this.findBlocksInside(blockName) || []).forEach(function(block) {
                    if(name && block.getName() !== name) return;
                    rez.push(block);
                });
            }, this);

            return rez;
        },

        /**
         * Очищает поле/всю форму
         * @param {String} [name] Имя поля формы
         */
        clear : function(name) {
            // TODO: так делать нельзя, в чекбоксах удалятся value
            if(!name) this.domElem[0].reset();

            var blocks = this._getBlocksByName(name),
                nativeControls = this._getFormElementsByName(name).filter(function(control) {
                    return blocks.every(function(block) { return block && block.elem('control')[0] !== control; });
                });

            blocks.forEach(function(control) { this.__self.clear(control); }, this);
            nativeControls.forEach(function(control) { this.__self.clear(control); }, this);

            return this;
        },

        getControlsByName : function(name) {
            var _this = this,
                blocks = [];

            this.domElem.find('[name=' + name + ']').get().map(function(domNode) {
                blocks.push(_this.findBlockOutside($(domNode), ({
                    checkbox : 'checkbox-group',
                    'datetime-local' : 'input',
                    hidden : 'select',
                    radio : 'radio-group',
                    text : 'input',
                    textarea : 'textarea'
                })[domNode.type]) || domNode);
            });

            blocks = blocks.concat(this.findBlocksInside('select').filter(function(block) {
                return block.getName() === name;
            }));

            return blocks.filter(function(value, index, array) { return array.indexOf(value) === index; });
        },

        getVal : function() {
            var formElements = this.domElem[0].elements,
                parsedData = {};

            Array.prototype.forEach.call(formElements, function(control) {
                var value = ['checkbox', 'radio'].indexOf(control.type) > -1 ?
                    (control.checked ? control.value : undefined) :
                    control.type.indexOf('select-') === 0 ?
                        Array.prototype.filter.call(control, function(option) { return option.selected; })
                            .map(function(option) { return option.value; }) :
                        control.value;

                if(control.name) {
                    if(!parsedData.hasOwnProperty(control.name)) parsedData[control.name] = undefined;

                    // игнорируем шаг для не чекнутых чекбоксов и переключателей
                    if(!(['checkbox', 'radio'].indexOf(control.type) > -1 && value === undefined)) {
                        parsedData[control.name] = parsedData[control.name] !== undefined ?
                            (Array.isArray(parsedData[control.name]) ?
                                parsedData[control.name] :
                                [parsedData[control.name]]).concat(value) :
                            value;
                    }
                }
            });

            // селект не создает инпут когда ничего не выбрано
            this.findBlocksInside('select').forEach(function(select) {
                var name = select.getName(),
                    value = select.getVal();

                if(name && !formElements.namedItem(name)) parsedData[name] = value;
            });

            // добавить к отправляемым данным файлы
            this.findBlocksInside('dnd-attach').forEach(function(block) {
                parsedData[block.getName()] = block.getVal();
            });

            return parsedData;
        },

        /**
         * @param {Object} data Key - имя контрола, value - его значение(я)
         */
        setVal : function(data) {
            Object.keys(data).forEach(function(key) {
                this.getControlsByName(key).forEach(function(block) {
                    if(([].concat(data[key]))[0] === undefined) {
                        this.clear(key);
                        return this;
                    }

                    block.constructor._name ?
                        ['checkbox', 'radio'].indexOf(block.constructor._name) > -1 ?
                            block.setMod('checked', true) :
                            block.constructor._name === 'checkbox-group' ||
                                (block.constructor._name === 'select' && block.getMod('mode') === 'check') ?
                                block.setVal([].concat(data[key])) :
                                block.setVal(data[key]) :
                        block.type === 'radio' ?
                            $(block).val() === data[key] && (block.checked = true) :
                            $(block).val(data[key]);
                }, this);
            }, this);

            return this;
        },

        /**
         * Отправка данных формы в виде FormData объекта с помощью XHR.
         * @returns {Promise} Возвращаемый промис резолвится ответом от сервера.
         */
        submit : (function() {
            var defer;

            return function() {
                // Если форма все еще отправляется - вернуть промис запроса.
                if(defer && !defer.promise().isResolved()) return defer.promise();

                var _this = this,
                    xhr = new XMLHttpRequest(),
                    parsedData = this.getVal(),
                    formData = Object.keys(parsedData).reduce(function(prev, key) {
                        var values = (Array.isArray(parsedData[key]) ? parsedData[key] : [parsedData[key]]);

                        // TODO: undefined вместо ''?
                        // Добавить в пустой массив пустую строку, чтобы было что отправлять на сервер
                        if(!values.length) values.push('');
                        values = values.map(function(value) { return value === undefined ? '' : value; });

                        values.forEach(function(data) { prev.append(key, data); });

                        return prev;
                    }, new FormData());

                defer = vow.defer();

                // отработает в любом случае
                xhr.onloadend = function() {
                    _this.delMod('pending');
                };
                // TODO: добавить onabort?
                xhr.onload = function() {
                    if(this.status >= 200 && this.status < 300) {
                        defer.resolve.apply(defer, arguments);
                    } else {
                        defer.reject.apply(defer, arguments);
                    }
                };
                xhr.onerror = function() {
                    defer.reject.apply(defer, arguments);
                };
                xhr.upload.addEventListener('progress', function() {
                    defer.notify.apply(defer, arguments);
                }, false);

                this.setMod('pending', true);

                // browser must support XHR Level 2
                xhr.open('POST', this.domElem.prop('action'), true);
                // чтобы express понимал req.xhr === true
                xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
                xhr.send(formData);

                return defer.promise();
            };
        }())
    }, /** @lends form */{
        live : function() {
            this.liveBindTo('submit', function(e) {
                e.preventDefault();

                this.submit();
            });
        },

        /**
         * Удаляет значение из переданного контрола
         * @param {BEM|DOM} block БЭМ-блок или DOM-нода (для нативных контролов формы)
         * @returns {Object} Возвращает this
         */
        clear : function(block) {
            // нативные контролы
            if(!block.constructor._name) {
                if(['checkbox', 'radio'].indexOf(block.type) > -1) block.checked = false;
                else $(block).val('');

                return this;
            }

            var blockName = block.constructor._name;

            if(blockName === 'select') blockName = {
                block : 'select', modName : 'mode', modVal : block.getMod('mode')
            };
            if(['checkbox', 'radio'].indexOf(blockName) > -1) return block.delMod('checked');
            if(blockName === 'dnd-attach') return block.clear();

            block.setVal(({
                input : '',
                '.select_mode_check' : [],
                '.select_mode_radio-check' : undefined
            })[typeof blockName === 'object' ? block.buildSelector(blockName.modName, blockName.modVal) : blockName]);

            return this;
        }
    }));
});
