({
    block : 'page',
    title : 'Icon',
    mods : { theme : 'islands' },
    head : [
        { elem : 'css', url : '_simple.css' }
    ],
    scripts : [
        { elem : 'js', url : '_simple.js' }
    ],
    content : [
        { tag : 'h2', content : '_file-extension' },
        [
            '3ds', '7z', 'aac', 'aif', 'apk', 'app', 'asf', 'asp', 'asx', 'avi', 'cdr', 'cfm', 'cgi', 'cpp', 'crx',
            'csr', 'csv', 'cue', 'dds', 'dem', 'dll', 'dmg', 'dmp', 'dtd', 'dwg', 'exe', 'flv', 'fnt', 'fon', 'gbr',
            'gzip', 'html', 'ico', 'iff', 'iso', 'jpg', 'jsp', 'kml', 'kmz', 'lua', 'm3u', 'm4a', 'max', 'mdf', 'mid',
            'mim', 'mp4', 'mpa', 'nes', 'odb', 'odc', 'odt', 'odx', 'pdf', 'pif', 'pl', 'png', 'rtf', 'srt', 'svg',
            'sys', 'tiff', 'tmp', 'txt', 'vcd', 'vob', 'wma', 'wmv', 'wps', 'wsf', 'xhtml', 'xlr', 'xls', 'xlsx', 'xml'
        ].map(function(v) { return { block : 'icon', mods : { 'file-extension' : v }, attrs : { title : v } }; }),
        { tag : 'h2', content : '_type' },
        ['image', 'upload'].map(function(v) { return { block : 'icon', mods : { type : v }, attrs : { title : v } }; })
    ]
})
