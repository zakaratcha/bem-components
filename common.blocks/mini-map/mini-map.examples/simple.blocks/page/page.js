/** @class page */
modules.define('page', ['i-bem__dom'], function(provide, BEMDOM) {
    provide(BEMDOM.decl(this.name, /** @lends page.prototype */{
        onSetMod : {
            js : {
                inited : function() {
                    var map = this.findBlockInside('mini-map'),
                        img = this.findBlockInside('image');

                    map.on('change', function() {
                        img.domElem.css({
                            backgroundPosition : map.getVal().x * 100 + '% ' + map.getVal().y * 100 + '%'
                        });
                    });

                    var i = document.createElement('img');

                    i.src = img.domElem.css('background-image').match(/^url\(["']?(.+?)["']?\)$/)[1];
                    i.onload = function() {
                        map.setVal({ x : 0.5, y : 0.5 }).setThumbSize({
                            width : img.domElem.width() / i.width * map.domElem.width(),
                            height : img.domElem.height() / i.height * map.domElem.height()
                        });
                    };
                }
            }
        }
    }));
});

