([
    {
        tech : 'js',
        mustDeps : [{ block : 'i-bem', tech : 'bemhtml' }]
    },
    {
        shouldDeps : [
            {
                elems : ['paranja'],
                mods : { visibility : 'hidden' }
            }
        ]
    }
])
