block('calendar')(
    content()(function() {
        var _days = ['пн', 'вт', 'ср', 'чт', 'пт', 'сб', 'вс'],
            _months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь',
                'Ноябрь', 'Декабрь'];

        var now = new Date(),
            currentDay = now.getDate(),
        // 0-11
            currentMonthIndex = now.getMonth(),
        // данные календаря
            calendar = _months.slice(currentMonthIndex)
                .concat(_months.slice(0, currentMonthIndex))
                .map(function(value, index) {
                    return {
                        // имя месяца
                        caption : value,
                        // индекс месяца в году (0-11)
                        index : (index + currentMonthIndex) % 12
                    };
                });

        calendar = calendar.map(function(month) {
            // кол-во дней в месяце (1-31)
            month.nDays = new Date(now.getFullYear(), month.index + 1, 0).getDate();
            // индекс первого дня месяца в неделе (0(monday) - 6(sunday))
            month.firstDayIndex = (new Date(now.getFullYear(), month.index, 1).getDay() || 7) - 1;

            return month;
        });

        return [
            {
                elem : 'list',
                mix : { block : 'mini-map', js : { scrollStepY : 0.015 } },
                content : [
                    { block : 'mini-map', elem : 'thumb' },
                    calendar.map(function(month, index) {
                        return {
                            elem : 'item',
                            content : index > 0 && month.index === 0 ?
                                [
                                    month.caption,
                                    { elem : 'year', content : now.getFullYear() + 1 }
                                ] :
                                month.caption
                        };
                    })
                ]
            },
            {
                elem : 'dates',
                content : [
                    {
                        elem : 'day-names',
                        content : _days.map(function(dayCaption, index) {
                            return {
                                elem : 'day', elemMods : { weekend : index > 4 || undefined },
                                content : dayCaption
                            };
                        })
                    },
                    {
                        elem : 'visible',
                        content : {
                            elem : 'months',
                            content : calendar.map(function(month, index) {
                                return {
                                    elem : 'month',
                                    attrs : {
                                        'data-year' : now.getFullYear() + (month.index < index ? 1 : 0),
                                        'data-month' : month.index + 1
                                    },
                                    content : [
                                        {
                                            elem : 'month-name',
                                            elemMods : {
                                                startDay : month.firstDayIndex !== 0 ?
                                                    month.firstDayIndex + 1 :
                                                    undefined
                                            },
                                            content : month.caption
                                        },
                                        (new Array(month.nDays)).join(' ').split(' ').map(function(v, i) {
                                            return {
                                                elem : 'day',
                                                elemMods : {
                                                    'in-past' : index === 0 && i + 1 <
                                                        currentDay || undefined,
                                                    weekend : [5, 6].indexOf((month.firstDayIndex +
                                                        i) % 7) > -1 ?
                                                        true :
                                                        undefined
                                                },
                                                content : i + 1
                                            };
                                        })
                                    ]
                                };
                            })
                        }
                    }
                ]
            }
        ];
    }),
    js()(true)
);
